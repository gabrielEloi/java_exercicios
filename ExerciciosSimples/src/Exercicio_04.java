import java.util.Scanner;

/**
 * 
 * @author Gabriel
 *
 *Escreva um programa para calcular e imprimir o n�mero de l�mpadas necess�rias para iluminar um
determinado c�modo de uma resid�ncia. Dados de entrada: a pot�ncia da l�mpada utilizada (em
watts), as dimens�es (largura e comprimento, em metros) do c�modo. Considere que a pot�ncia
necess�ria � de 18 watts por metro quadrado.
 */

public class Exercicio_04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		float pot_lamp, larg_com, comp_com, area_com, pot_total;
		int num_lamp;
		System.out.println("Qual a potencia da lampada (em watts)?");
		pot_lamp = dig.nextFloat();
		System.out.println("Qual a largura do comodo (em metros)?");
		larg_com = dig.nextFloat();
		System.out.println("Qual o comprimento do comodo (em metros)?");
		comp_com = dig.nextFloat();
		
		area_com = larg_com * comp_com;
		pot_total = area_com * 18;
		num_lamp = Math.round(pot_total/pot_lamp);
		System.out.println("N�mero de lampadas necessarias para iluminar esse comodo: "+num_lamp);
	}

}
