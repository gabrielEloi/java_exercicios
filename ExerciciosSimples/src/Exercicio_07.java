import java.util.Scanner;
/**
 * 
 * @author Gabriel
 * 
 *A equipe Benneton-Ford deseja calcular o n�mero m�nimo de litros que dever� colocar no tanque
de seu carro para que ele possa percorrer um determinado n�mero de voltas at� o primeiro
reabastecimento. Escreva um programa que leia o comprimento da pista (em metros), o n�mero total
de voltas a serem percorridas no grande pr�mio, o n�mero de reabastecimentos desejados e o
consumo de combust�vel do carro (em Km/L). Calcular e escrever o n�mero m�nimo de litros
necess�rios para percorrer at� o primeiro reabastecimento. OBS: Considere que o n�mero de voltas
entre os reabastecimentos � o mesmo.
 */
public class Exercicio_07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		
		float compr,cons,qt_min,periodo,percurso;
		int voltas, reabas;
		
		System.out.println("Informe o comprimento da pista: ");
		compr = dig.nextFloat();
		System.out.println("Informe o numero de voltas a serem percorridas: ");
		voltas = dig.nextInt();
		System.out.println("Informe o numero de reabastecimentos desejados: ");
		reabas = dig.nextInt();
		System.out.println("Informe o consumo de combustivel(em km/L): ");
		cons = dig.nextFloat();
		
		percurso = (compr * voltas)/1000;
		
		periodo = percurso/reabas;
		
		qt_min = periodo/cons;
		
		System.out.println("O numero minimo de litros necessarios para percorrer ate o primeiro reabastecimento �: "+qt_min);
		
	}

}
