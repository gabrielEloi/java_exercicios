import java.util.Scanner;
/**
 * 
 * @author Gabriel
 * 
 *Escreva um programa para ler as dimens�es de uma cozinha retangular (comprimento, largura e
altura), calcular e escrever a quantidade de caixas de azulejos para se colocar em todas as suas
paredes (considere que n�o ser� descontada a �rea ocupada por portas e janelas). Cada caixa de
azulejos possui 1,5 m2.
 */
public class Exercicio_05 {
	public static int calcCx(float ar) {
		return (int) Math.round(ar/1.5);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		float comp=0, larg=0, alt=0, area=0;
		int caixas=0;
		System.out.println("Qual o comprimento da cozinha? ");
		comp = dig.nextFloat();
		System.out.println("Qual a largura da cozinha? ");
		larg = dig.nextFloat();
		System.out.println("Qual a altura da cozinha? ");
		alt = dig.nextFloat();
		
		area = (comp*alt*2) + (larg*alt*2);
		//caixas = (int) Math.round(area/1.5);
		
		System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: "+calcCx(area));
		
		

	}

}
