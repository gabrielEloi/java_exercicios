import java.util.Scanner;
/**
 * 
 * @author Gabriel
 *
 *Um motorista de t�xi deseja calcular o rendimento de seu carro na pra�a. Sabendo-se que o pre�o
do combust�vel � de R$ 1,90, escreva um programa para ler: a marca��o do od�metro (Km) no in�cio
do dia, a marca��o (Km) no final do dia, o n�mero de litros de combust�vel gasto e o valor total (R$)
recebido dos passageiros. Calcular e escrever: a m�dia do consumo em Km/L e o lucro (l�quido) do
dia.
 */
public class Exercicio_06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		float odom_i, odom_f, litros, valor_t, media, lucro, gasol_l;
		gasol_l = (float) 1.90;
		System.out.println("Marca��o inicial do odometro (km): ");
		odom_i = dig.nextFloat();
		System.out.println("Marca��o final do odometro (km): ");
		odom_f = dig.nextFloat();
		System.out.println("Quantidade de combustivel gasto (litros)");
		litros = dig.nextFloat();
		System.out.println("Valor total recebido (R$): ");
		valor_t = dig.nextFloat();
		
		//media = (odom_f - odom_i) / litros;
		//lucro = valor_t - (litros * gasol_l);
		
		System.out.println("Media de consumo em Km/L: "+media(odom_f,odom_i,litros));
		System.out.println("Lucro (Liquido) do dia: R$"+lucro(valor_t,litros,gasol_l));
		
	}
	public static float media(float odf,float odi,float lt) {
		return (odf-odi)/lt;
	}
	public static float lucro(float vt, float lt, float gas) {
		return vt - (lt*gas);
	}

}
