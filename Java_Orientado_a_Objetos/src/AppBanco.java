import java.util.Scanner;

public class AppBanco {
	
	static final int mx = 10;
	
	static int cont = 0;
	
	static Conta[] lista = new Conta[mx];
	
	static Scanner dig = new Scanner(System.in);

	public static void main(String[] args) {
		
		int op=0;
		
		do {
			System.out.println("MENU");
			System.out.println("1-Criar conta");
			System.out.println("2-Debitar");
			System.out.println("3-Creditar");
			System.out.println("4-Excluir conta");
			System.out.println("5-Consultar Conta");
			System.out.println("6-Sair");
			op = dig.nextInt();
			switch (op) {
			case 1:criarConta(); break;
			case 2:debitarValor(); break;
			case 3:creditarValor(); break;
			case 4:ExcluirConta(); break;
			case 5:consultarLista(); break;
			case 6:break;
			
			}
		} while (op != 6);
		
	}
	
	public static void criarConta() {
		System.out.println("Digite o numero da conta: ");
		int n = dig.nextInt();
		System.out.println("Digite o saldo da conta: ");
		double s = dig.nextDouble();
		
		lista[cont++] = new Conta(n,s);
		System.out.println("Conta criada com sucesso");
	}
	
	public static void debitarValor() {
		System.out.println("Digite o numero da Conta: ");
		int n = dig.nextInt();
		System.out.println("Digite o valor Debitado: ");
		int v = dig.nextInt();
		
		for (int i = 0; i < lista.length-1; i++) {
			if (n == lista[i].getNumero()) {
				lista[i].debitar(v);
				break;
			}
		}
	}
	public static void creditarValor() {
		System.out.println("Digite o numero da conta");
		int n = dig.nextInt();
		System.out.println("Digite o valor Creditado: ");
		double v = dig.nextDouble();
		
		for (int i = 0; i < lista.length-1; i++) {
			if (n == lista[i].getNumero()) {
				lista[i].creditar(v);
				break;
			}
		}
	
	}
	public static void consultarLista() {
		System.out.println("N� Conta:.................Salddo:");
		for (int i = 0; i < lista.length-1; i++) {
			if (lista[i] != null) {
				System.out.println(lista[i].getNumero()+".........."+lista[i].getSaldo());
				
			}else {
				break;
			}
		}
	}
	
	public static void ExcluirConta() {
		System.out.println("Digite o numero da conta que sera excluida: ");
		int n = dig.nextInt();
		
		for (int i = 0; i < lista.length-1; i++) {
			if (n == lista[i].getNumero()) {
				lista[i] = null;
			}
		}
	}

}
