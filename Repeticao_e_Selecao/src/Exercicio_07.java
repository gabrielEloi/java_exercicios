import java.util.Scanner;

public class Exercicio_07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner dig = new Scanner(System.in);
		
		float N1=0,N2=0,MD;
		int esc=1;
		
		do {
			do{
				System.out.println("Digite o valor da 1� nota: ");
				N1 = dig.nextFloat();
				if(N1<0 || N1>10) {
					System.out.println("Nota invalida");
				}
			}while(N1<0 || N1>10);
			
			do {
				System.out.println("Digite o valor da 2� nota: ");
				N2 = dig.nextFloat();
				if(N2<0 || N2>10) {
					System.out.println("Nota invalida");
				}
			} while (N2<0 || N2>10);
			MD = (N1+N2)/2;
			System.out.println("O valor da M�dia �: "+MD);
			
			System.out.println("Novo Calculo (1:sim e 2:n�o)");
			esc = dig.nextInt();
			while (esc < 1 || esc > 2) {
				System.out.println("Novo Calculo (1:sim e 2:n�o)");
				esc = dig.nextInt();
			}
			if(esc == 1) {
				N1=0;N2=0;
			}
		} while (esc==1);
		
		
	}

}
