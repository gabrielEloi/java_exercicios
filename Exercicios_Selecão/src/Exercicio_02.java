import java.util.Scanner;

public class Exercicio_02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		
		float nota1, nota2,opt = 0, media;
		int esc;
		
		System.out.println("Digite a nota da primeira avalia��o: ");
		nota1 = dig.nextFloat();
		System.out.println("Digite a nota da segunda avalia��o: ");
		nota2 = dig.nextFloat();
		
		System.out.println("Deseja fazer a avalia��o optativa(1 = sim || 2 = n�o)");
		esc = dig.nextInt();
		
		if(esc == 1) {
			System.out.println("Digite a nota da avalia��o optativa: ");
			opt = dig.nextFloat();
		}else if(esc == 2) {
			opt = -1;
		}
		
		if(nota1 > nota2) {
			if(opt > nota2) {
				nota2 = opt;
			}
		}else if(nota2 > nota1) {
			if(opt > nota1) {
				nota1 = opt;
			}
		}
		
		media = (nota1 + nota2)/2;
		
		if(media >= 6) {
			System.out.println("Aprovado");
		}else if(media < 3) {
			System.out.println("Reprovado");
		}else if(media >= 3 && media < 6) {
			System.out.println("Exame");
		}
		
	}

}
