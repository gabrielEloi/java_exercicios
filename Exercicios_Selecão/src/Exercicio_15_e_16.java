import java.util.Scanner;

public class Exercicio_15_e_16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		
		int n_lado;
		float m_lado;
		
		System.out.println("Quantos lados tem o poligono: ");
		n_lado = dig.nextInt();
		System.out.println("Qual a medida do lado(cm): ");
		m_lado = dig.nextFloat();
		if (n_lado < 3) {
			System.out.println("Não é um poligono");
		}
		else if (n_lado == 3) {
			System.out.println("Triangulo");
			System.out.println("Perimetro: "+(m_lado * 3));
		}else if (n_lado == 4) {
			System.out.println("Quadrado");
			System.out.println("Area: "+(m_lado * m_lado));
		}else if (n_lado == 5){
			System.out.println("Pentagono");
		}else {
			System.out.println("Poligono não identificado");
		}
	}

}
