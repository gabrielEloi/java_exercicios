import java.util.Scanner;

public class Exercicio_06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		
		float valor;
		
		System.out.println("Digite um valor: ");
		valor = dig.nextFloat();
		
		if(valor<0) {
			System.out.println("Negativo");
		}else if(valor > 0) {
			System.out.println("Positivo");
		}else if (valor == 0) {
			System.out.println("ZERO");
		}
	}

}
