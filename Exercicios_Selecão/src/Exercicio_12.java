import java.util.Scanner;

public class Exercicio_12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dig = new Scanner(System.in);
		
		float altura,peso;
		int sexo;
		
		System.out.println("Digite sua altura: ");
		altura = dig.nextFloat();
		System.out.println("Sexo: 1-Feminino,2-Masculino");
		sexo = dig.nextInt();
		
		if (sexo == 1) {
			peso = (float) ((62.1 * altura) - 44.7);
			System.out.println("Seu peso ideal é: "+peso);
		}else {
			peso = (float) ((72.7 * altura) - 58);
			System.out.println("Seu peso ideal é: "+peso);
		}
	}

}
